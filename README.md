This application is to demonstrate achieving no downtime (Blue Green) deployments using Spring cloud framework

**A typical use case**
--
**Scenario**: We want to build a web application that needs to communicate with various other systems and microservices to perform its task</br>

**Requirements**:
- Need to achieve no downtime deployments. Application can be deployed any time and switched to new version any time afterward. Rollback is simply switching to older version
- No hardcoding of microservices url/dns in the yml files
- Resiliency 

**Things you will need to know for this project**
--
- Spring boot  - For rest services
- Spring Cloud Eureka - for building a Service Discovery server
- Spring Cloud Gateway -  To redirect requests

**Modules in this project**
--
1. [spring-apis](./spring-apis) :  This is a basic application to mock application api server. Two run configuration are saved as part of this project to run two instance of same services on port 9000 and 9001.
2. [discovery-server](./discovery-server) : this is a discovery server that knows about all the services. Any client can ask for an instance of a service using this discovery server. Using eureka client the [spring-apis](./spring-apis) will register themselves to this discovery server on startup.</br>
    if one instance of the api server goes down discovery server will provide the instance of api server which is up. This is usually done by application pings. The registering client pings discovery server after every 30 seconds (default) to let it know about state of the services
3. [spring-gateway](./spring-gateway) : This gateway sits between client and the server calls. It can almost entirely manged using configs. </br>
    The server get all the incoming requests from clients and then based on configuration redirects them to appropriate service. In this example instead of hardcoding the spi service location the config make use of discovery server to know about actual location of the service

**Application Setup**
--
- Multiple load balanced instance of gateway. Interaction with application in production environment is using Gateway url only. (Even though direct url can also be configured side by side)
- Multiple instance of service discovery. Each instance is aware of each other. This provides resiliency if one of the discovery server node goes down
- Multiple instance of the actual applications. Applications will register itself with discovery server

With this setup we can achieve Green blue deployments. i.e. We can deploy a new version of the application withing brining down old application. When we are ready to go live we redirect the traffic to new instance using gateway. </br>
The redirection can be configured using a config server, or via a separate pipeline where the team perform the switch to new gateway configuration (identified using a unique id)




