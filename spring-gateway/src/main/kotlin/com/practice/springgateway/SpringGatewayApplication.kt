package com.practice.springgateway

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.client.discovery.EnableDiscoveryClient

@SpringBootApplication
@EnableDiscoveryClient
class SpringGatewayApplication

fun main(args: Array<String>) {
  runApplication<SpringGatewayApplication>(*args)
}
