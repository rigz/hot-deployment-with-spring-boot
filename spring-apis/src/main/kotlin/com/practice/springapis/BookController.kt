package com.practice.springapis

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api")
class BookController {
  @GetMapping("books")
  fun get():List<Book> = listOf(Book("Master .net"), Book("Effective Java"))
}

data class Book(val name:String)
